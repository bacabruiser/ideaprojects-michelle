var express = require('express');
var router = express.Router();

/* GET about page. */
router.get('/', function(req, res, next) {
    res.render('about', {
        title: 'About Me',
        name: "Michelle"
    });
});

module.exports = router;
