var express = require('express');
var router = express.Router();
var vd = require('../videodata.json')

/* GET about page. */
router.get('/', function(req, res, next) {
  res.render('index', {
    title: 'Express',
    name: "Michelle",
    videodata: vd
  });
});

module.exports = router;
